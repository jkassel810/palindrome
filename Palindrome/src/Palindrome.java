/**
 * Created by jkass on 11/26/2016.
 *
 * Check if Palindrome – Checks if the string entered by the user is a palindrome.
 * That is that it reads the same forwards as backwards like “racecar”
 */
public class Palindrome {

    public static boolean checkPalindrome(String myString){

        char[] s2char = myString.toCharArray();
        StringBuffer sb = new StringBuffer(myString);
        boolean isPalindrome = false;
        if (myString.equalsIgnoreCase(sb.reverse().toString())){
            isPalindrome = true;
        }

        return isPalindrome;
    }


    public static void main(String[] args){

        String testString = "radar";

        boolean testPalindrome = checkPalindrome(testString);

        System.out.println(testString + " is a Palindrome?: " + testPalindrome);

        String testString2 = "Testing";

        boolean testPalindrome2 =checkPalindrome(testString2);

        System.out.println(testString2 + " is a Palindrome?: " + testPalindrome2);

    }



}
